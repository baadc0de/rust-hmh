pub struct DoubleBuffer<T>
where
    T: Copy,
{
    state: [T; 2],
    old: u8,
    new: u8,
}

impl<T> DoubleBuffer<T>
where
    T: Clone + Copy,
{
    pub fn init(init_value: T) -> Self {
        Self {
            state: [init_value.clone(), init_value],
            old: 0,
            new: 0,
        }
    }

    pub fn copy(&mut self) {
        self.state[self.new as usize] = self.state[self.old as usize];
    }

    pub fn flip(&mut self) {
        std::mem::swap(&mut self.old, &mut self.new);
    }

    #[allow(dead_code)]
    pub fn old(&self) -> &T {
        &self.state[self.old as usize]
    }

    pub fn new(&mut self) -> &mut T {
        &mut self.state[self.new as usize]
    }
}
