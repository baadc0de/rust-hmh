//
// Handmade Hero (Rust version)
//

mod double_buffer;

use double_buffer::DoubleBuffer;
use gilrs::{Axis, Button, Event as GamepadEvent, EventType, Gilrs};
use pixels::{Pixels, PixelsBuilder, SurfaceTexture};
use pixels_u32::PixelsExt;
use winit::{
    dpi::PhysicalSize,
    event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

//
// Constants
//

const GAME_WIDTH: u32 = 800;
const GAME_HEIGHT: u32 = 600;
const WINDOW_WIDTH: u32 = GAME_WIDTH * 1;
const WINDOW_HEIGHT: u32 = GAME_HEIGHT * 1;

//
// Global state
//

struct Globals {
    x: i32,
    y: i32,
    gamepads: Gilrs,

    input_state: DoubleBuffer<InputState>,
}

#[derive(Debug, Clone, Copy)]
struct InputState {
    dpad_up: bool,
    dpad_down: bool,
    dpad_left: bool,
    dpad_right: bool,
    start: bool,
    back: bool,
    a: bool,
    b: bool,
    x: bool,
    y: bool,
    left_shoulder: bool,
    right_shoulder: bool,
    stick_x: f32,
    stick_y: f32,
}

impl InputState {
    fn new() -> Self {
        Self {
            dpad_up: false,
            dpad_down: false,
            dpad_left: false,
            dpad_right: false,
            start: false,
            back: false,
            a: false,
            b: false,
            x: false,
            y: false,
            left_shoulder: false,
            right_shoulder: false,
            stick_x: 0.0,
            stick_y: 0.0,
        }
    }
}

//
// Main entry point
//

fn main() -> anyhow::Result<()> {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("Handmade Hero")
        .with_inner_size(PhysicalSize::new(WINDOW_WIDTH, WINDOW_HEIGHT))
        .with_resizable(false)
        .build(&event_loop)?;
    let mut pixels = {
        let surface_texture = SurfaceTexture::new(WINDOW_WIDTH, WINDOW_HEIGHT, &window);
        PixelsBuilder::new(GAME_WIDTH as u32, GAME_HEIGHT as u32, surface_texture)
            .enable_vsync(false)
            .build()?
    };

    let mut globals = Globals {
        x: 0,
        y: 0,
        gamepads: Gilrs::new().unwrap(),
        input_state: DoubleBuffer::<InputState>::init(InputState::new()),
    };

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::WindowEvent { window_id, event } if window_id == window.id() => match event {
                WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode: Some(VirtualKeyCode::Escape),
                            ..
                        },
                    ..
                } => {
                    *control_flow = ControlFlow::Exit;
                }
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                _ => {}
            },
            Event::RedrawRequested(window_id) if window_id == window.id() => {
                draw(&mut pixels, globals.x as usize, globals.y as usize)
            }
            Event::MainEventsCleared => {
                // Get input
                get_input(&mut globals);
                update(&mut globals);
                window.request_redraw();
                globals.input_state.flip();
            }
            _ => {}
        }
    })
}

//
// Update

fn update(globals: &mut Globals) {
    globals.x += 1;
    if globals.input_state.new().a {
        globals.y += 1;
    }
}

//
// Rendering
//

fn draw(pixels: &mut Pixels, offset_x: usize, offset_y: usize) {
    // Format of pixels is AABBGGRR
    let frame = pixels.get_frame_u32();

    frame
        .chunks_mut(GAME_WIDTH as usize)
        .enumerate()
        // Loop through the rows
        .for_each(|(i, row)| {
            // Loop through the columns in each row.
            row.iter_mut().enumerate().for_each(|(x, p)| {
                *p = colour(
                    0,
                    ((i + offset_y) % 256) as u8,
                    ((x + offset_x) % 256) as u8,
                );
            })
        });

    pixels.render().unwrap_or_default();
}

/// Returns a colour in the format of AABBGGRR
fn colour(red: u8, green: u8, blue: u8) -> u32 {
    let red = red as u32;
    let green = (green as u32) << 8;
    let blue = (blue as u32) << 16;
    0xff000000 | red | green | blue
}

//
// Input
//

fn get_input(globals: &mut Globals) {
    globals.input_state.copy();
    let mut input = globals.input_state.new();
    // while let Some(GamepadEvent { event, .. }) =
    //     gilrs::ev::filter::axis_dpad_to_button(globals.gamepads.next_event(), &mut globals.gamepads)
    while let Some(GamepadEvent { event, .. }) = globals.gamepads.next_event() {
        match event {
            EventType::ButtonPressed(button, _) => match button {
                Button::DPadUp => input.dpad_up = true,
                Button::DPadDown => input.dpad_down = true,
                Button::DPadLeft => input.dpad_left = true,
                Button::DPadRight => input.dpad_right = true,
                Button::South => input.a = true,
                Button::East => input.b = true,
                Button::West => input.x = true,
                Button::North => input.y = true,
                Button::Select => input.back = true,
                Button::Start => input.start = true,
                Button::LeftTrigger => input.left_shoulder = true,
                Button::RightTrigger => input.right_shoulder = true,
                _ => {}
            },
            EventType::ButtonReleased(button, _) => match button {
                Button::DPadUp => input.dpad_up = false,
                Button::DPadDown => input.dpad_down = false,
                Button::DPadLeft => input.dpad_left = false,
                Button::DPadRight => input.dpad_right = false,
                Button::South => input.a = false,
                Button::East => input.b = false,
                Button::West => input.x = false,
                Button::North => input.y = false,
                Button::Select => input.back = false,
                Button::Start => input.start = false,
                Button::LeftTrigger => input.left_shoulder = false,
                Button::RightTrigger => input.right_shoulder = false,
                _ => {}
            },
            EventType::AxisChanged(axis, value, _) => match axis {
                Axis::LeftStickX => input.stick_x = value,
                Axis::LeftStickY => input.stick_y = value,
                _ => {}
            },
            _ => {}
        }
    }
}
